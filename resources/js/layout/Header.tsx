import React, { useState } from 'react';
import { Dropdown, Modal, Button } from 'react-bootstrap';
import { useHistory, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import UserForm from '../views/users/Form';
import { logout as logoutAction, clearAuthError } from '../actions/auth';
import { selectAuthenticatedUser } from '../selectors/auth';

type PropsToggle = {
    onClick: (e: any) => void,
}
type PropsMenu = {
    className: string,
}

export default function Header() {
    const dispatch = useDispatch()
    const user = useSelector(selectAuthenticatedUser)
    const modalBodyStyle = {
        maxHeight: window.innerHeight - 200
    }
    const [showModal, setShowModal] = useState<boolean>(false);
    const closeProfileModal = () => {
        dispatch(clearAuthError());
        setShowModal(false);
    }
    const showProfileModal = () => setShowModal(true);
    const history = useHistory();
    const location = useLocation();
    const { from } = location.state || { from: { pathname: "/login" } };
    const logout = function () {
        dispatch(logoutAction());
        history.replace(from);
    }
    const CustomToggle = React.forwardRef<HTMLAnchorElement, PropsToggle>(({ children, onClick }, ref) => (
        <a className="nav-link" role="button" href="#" ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}>
            {children}
        </a>
    ));
    const CustomMenu = React.forwardRef<HTMLUListElement, PropsMenu>(
        ({ children, className }, ref) => {
            return (
                <ul ref={ref}  className={className} >
                    {React.Children.toArray(children)}
                </ul>
            );
        },
    );
    const onPushMenu = (e) => {
        e.preventDefault()
        let body = document.getElementsByTagName('body')[0]
        if (body.classList.contains('sidebar-open')) {
            body.classList.remove('sidebar-open')
            body.classList.add('sidebar-collapse')
        }
        else {
            body.classList.remove('sidebar-collapse')
            body.classList.add('sidebar-open')
        }
    }

    return (
        <>
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" data-widget="pushmenu" href="#" onClick={onPushMenu}>
                            <i className="fas fa-bars"></i>
                        </a>
                    </li>
                </ul>
                <div className="navbar-nav ml-auto">
                    <Dropdown className="nav-item user-menu">
                        <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                            <img src={user && user.avatar ? user.avatar.path : "/avatar.png"}
                                className="user-image img-circle elevation-2" alt="User" />
                            <span className="d-none d-md-inline mr-2">{user?.fullName}</span>
                        </Dropdown.Toggle>
                        <Dropdown.Menu as={CustomMenu} className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <li className="user-header bg-primary">
                                <img src={user && user.avatar ? user.avatar.path : "/avatar.png"} className="img-circle elevation-2" alt="User" />
                                <p>
                                    {user?.fullName}
                                    <small>Miembro desde {user.registrationDate}</small>
                                </p>
                            </li>
                            <li className="user-footer">
                                <a href="#" className="btn btn-default btn-flat"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        showProfileModal();
                                    }}>Perfil</a>
                                <a href="#" className="btn btn-default btn-flat float-right"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        logout();
                                    }}>Cerrar sesión</a>
                            </li>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </nav>
            <Modal show={showModal} onHide={closeProfileModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Datos del perfil</Modal.Title>
                </Modal.Header>
                <Modal.Body style={modalBodyStyle}>
                    <UserForm id="profileForm" userData={{...user}} mode="profile" />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" form="profileForm">
                        Guardar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
