import React from 'react';
import Header from './Header';
import Sidebar from './Sidebar';
import { Switch } from "react-router-dom";
import RouteWithSubRoutes from '../utils/RouteWithSubRoutes';
import { IRoute } from '../routes';

interface IMainProps {
    routes: IRoute[];
}
export const Main: React.FC<IMainProps> = ({ routes }) => {
    const body = document.getElementsByTagName('body')[0]
    body.classList.remove('login-page')
    body.classList.add('sidebar-mini', 'sidebar-open', 'layout-fixed')
    if (window.innerWidth < 992) {
        body.classList.add('sidebar-collapse')
    }
    const closeSidebar = () => {
        body.classList.remove('sidebar-open')
        body.classList.add('sidebar-collapse')
    }
    return (
        <div className="main-container">
            <Header />
            <Sidebar />
            <div className="content-wrapper">
                <Switch>
                    {routes.map((route, i) => (
                        <RouteWithSubRoutes key={i} {...route} />
                    ))}
                </Switch>
            </div>
            <div id="sidebar-overlay" onClick={closeSidebar}></div>
        </div>
    );
};
