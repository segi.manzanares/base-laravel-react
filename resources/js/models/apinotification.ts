import { IApiError } from './apierror';

export interface IApiNotification {
    type: "error" | "success";
    message: string;
    error?: IApiError;
}