import { IRole, Role } from './role';
import { ImageData } from '../components/ImageCropper';
import { IImage } from './image';
import moment from 'moment';

export type TUserForm = {
    id?: number;
    first_name: string;
    last_name: string;
    email: string;
    avatar_file?: ImageData;
    avatar_path?: string;
    role_id?: number;
    password?: string;
    password_confirmation?: string;
}
export interface IUser {
    id?: number;
    first_name: string;
    last_name: string;
    email: string;
    is_active: boolean;
    avatar?: IImage;
    avatar_file?: ImageData;
    avatar_path?: string;
    created_at: string;
    role?: IRole;
    role_id?: number;
    password?: string;
    password_confirmation?: string;
}

export class User implements IUser {
    constructor(
        public id: number,
        public first_name: string,
        public last_name: string,
        public email: string,
        public is_active: boolean,
        public created_at: string,
        public role?: Role,
        public avatar?: IImage,
        public avatar_file?: ImageData,
    ) { }

    public static fromJson(data: IUser): User {
        return new User(
            data['id'] ? data.id : 0,
            data['first_name'],
            data['last_name'],
            data['email'],
            data['is_active'],
            data['created_at'],
            data['role'] ? Role.fromJson(data['role']) : void 0,
            data['avatar'] ? data.avatar : void 0,
            data['avatar_file'],
        );
    }

    get fullName(): string {
        return this.getFullName();
    }

    public getFullName(): string {
        return this.first_name + ' ' + this.last_name;
    }

    get registrationDate(): string {
        let date = new Date(this.created_at);
        return moment(new Date(this.created_at)).format('DD/MMM/YYYY')
    }
}