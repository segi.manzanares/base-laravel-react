export interface IApiError<T = any> {
    status: number;
    code?: string;
    message: string;
    errors?: { [P in keyof T]?: string[] };
}

