import { IApiError } from './models/apierror';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';

const apiBaseUrl: string = process.env.MIX_REACT_APP_API_URL;

export default function (method: string, url: string, data: Object = null, config: Object = null) {
    if (data == null) {
        data = {};
    }
    if (config == null) {
        config = {};
    }
    var options: AxiosRequestConfig = {};
    if (config['headers'] !== void 0) {
        options.headers = config['headers'];
        if (method === 'put' && options.headers.hasOwnProperty('Content-Type')
            && options.headers['Content-Type'] === 'multipart/form-data') {
            method = 'post';
            if (data instanceof FormData) {
                data.append('_method', 'put');
            }
        }
    }
    if (method === 'get') {
        if (data) {
            url += '?';
            Object.keys(data).forEach(key => {
                if (data[key] !== null) {
                    url += key + '=' + data[key] + '&';
                }
            });
        }
    }
    return axios[method](apiBaseUrl + url, data, options)
        .then(response => response)
        .catch((error: AxiosError) => {
            let parsedError: IApiError = {
                status: error.response.status,
                code: error.response.data?.code,
                errors: error.response.data?.errors,
                message: error.response.data?.message,
            }
            if (parsedError.status === 429) {
                parsedError.message = "Se excedió el límite de peticiones al API. Vuelva a intentarlo en 1 minuto.";
            }
            else if (parsedError.status === 500) {
                parsedError.message = "An error occurred on the server when your request was being processed, we will solve the problem as soon as possible.";
            }
            return Promise.reject(parsedError);
        });
}