import { IApiNotification } from '../models/apinotification';

export interface IErrorState {
    notification: IApiNotification;
    isShowing: boolean;
}

export const initialState: IErrorState = {
    notification: null,
    isShowing: false
};

export function reducer(
    state = initialState,
    action: any
): IErrorState {
    if (action.notification) {
        return { notification: action.notification, isShowing: true }
    }
    return state
}