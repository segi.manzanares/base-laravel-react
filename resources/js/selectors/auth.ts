import { createSelector } from "reselect"
import { User } from "../models/user"
import { RootState } from "../reducers"

const selectAuthState = createSelector(
    (state: RootState) => state.auth,
    auth => auth
)

export const selectAuthenticatedUser = createSelector(
    selectAuthState,
    auth => auth ? User.fromJson(auth.auth.user) : null
)