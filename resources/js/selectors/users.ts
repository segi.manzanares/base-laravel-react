import { createSelector } from "reselect"
import { User } from "../models/user"
import { RootState } from "../reducers"

const selectUsersState = createSelector(
    (state: RootState) => state.users,
    users => users
)

export const selectUsers = createSelector(
    selectUsersState,
    users => users ? users.data.map(data => User.fromJson(data)) : []
)

export const isLoading = createSelector(
    selectUsersState,
    users => users ? users.isLoading : false
)