import { IApiNotification } from './../models/apinotification';
import { Dispatch } from 'react';
import { IUser } from './../models/user';
import { IAuth, TAuthForm } from './../models/auth';
import { IApiError } from './../models/apierror';
import { AppThunk } from '../store';
import api from '../api';

export const REQUEST_CLIENT_TOKEN = 'REQUEST_CLIENT_TOKEN'
export const REQUEST_CLIENT_TOKEN_SUCCESS = 'REQUEST_CLIENT_TOKEN_SUCCESS'
export const REQUEST_CLIENT_TOKEN_ERROR = 'REQUEST_CLIENT_TOKEN_ERROR'
export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGOUT = 'LOGOUT'
export const UPDATE_PROFILE = 'UPDATE_PROFILE'
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS'
export const UPDATE_PROFILE_ERROR = 'UPDATE_PROFILE_ERROR'
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD'
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS'
export const FORGOT_PASSWORD_ERROR = 'FORGOT_PASSWORD_ERROR'
export const RESET_PASSWORD = 'RESET_PASSWORD'
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS'
export const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR'
export const CLEAR_AUTH_ERROR = 'CLEAR_AUTH_ERROR'

interface RequestClientToken {
    type: typeof REQUEST_CLIENT_TOKEN;
}
export function requestClientToken(): RequestClientToken {
    return {
        type: REQUEST_CLIENT_TOKEN,
    }
}

interface RequestClientTokenError {
    type: typeof REQUEST_CLIENT_TOKEN_ERROR;
    error: IApiError<TAuthForm>;
}
export function requestClientTokenError(error: IApiError<TAuthForm>): RequestClientTokenError {
    return {
        type: REQUEST_CLIENT_TOKEN_ERROR,
        error
    }
}

interface RequestClientTokenSuccess {
    type: typeof REQUEST_CLIENT_TOKEN_SUCCESS;
    auth: IAuth;
}
export function requestClientTokenSuccess(auth: IAuth): RequestClientTokenSuccess {
    return {
        type: REQUEST_CLIENT_TOKEN_SUCCESS,
        auth,
    }
}

function shouldRequestClientToken(auth: IAuth) {
    if (!auth) {
        return true
    }
    return auth.expires_at <= Date.now()
}

export function requestClientTokenIfNeeded(): AppThunk {
    return (dispatch, getState) => {
        if (shouldRequestClientToken(getState().auth.auth)) {
            return dispatch(fetchClientToken())
        }
    }
}

export function fetchClientToken() {
    return (dispatch: Dispatch<AuthActions>) => {
        dispatch(requestClientToken())
        let data = {
            grant_type: 'client_credentials',
            client_id: process.env.MIX_REACT_APP_API_CLIENT_ID,
            client_secret: process.env.MIX_REACT_APP_API_CLIENT_SECRET,
        };
        return api('post', '/oauth/token', data)
            .then(response => {
                dispatch(requestClientTokenSuccess(response.data))
            })
            .catch(err => dispatch(requestClientTokenError(err)))
    }
}

interface Login {
    type: typeof LOGIN;
    email: string;
    password: string;
}
export function login(email: string, password: string): Login {
    return {
        type: LOGIN,
        email,
        password,
    }
}

export function performLogin(email: string, password: string) {
    return (dispatch: Dispatch<AuthActions>) => {
        dispatch(login(email, password));
        let data = {
            username: email,
            password,
            grant_type: 'password',
            client_id: process.env.MIX_REACT_APP_API_CLIENT_ID,
            client_secret: process.env.MIX_REACT_APP_API_CLIENT_SECRET,
        };
        return api('post', '/oauth/token', data)
            .then(response => {
                return response;
            })
            .then(response => dispatch(loginSuccess(response.data)))
            .catch(err => dispatch(loginError(err)))
    }
}

interface LoginSuccess {
    type: typeof LOGIN_SUCCESS;
    auth: IAuth;
}
export function loginSuccess(auth: IAuth): LoginSuccess {
    return {
        type: LOGIN_SUCCESS,
        auth
    }
}

interface LoginError {
    type: typeof LOGIN_ERROR;
    error: IApiError<TAuthForm>;
}
export function loginError(error: IApiError<TAuthForm>): LoginError {
    return {
        type: LOGIN_ERROR,
        error
    }
}

interface Logout {
    type: typeof LOGOUT;
}
export function logout(): Logout {
    return {
        type: LOGOUT,
    }
}

interface UpdateProfile {
    type: typeof UPDATE_PROFILE;
    userData: IUser;
}
export function updateProfile(userData: IUser): UpdateProfile {
    return {
        type: UPDATE_PROFILE,
        userData
    }
}

export function performUpdateProfile(userData: IUser) {
    return (dispatch: Dispatch<AuthActions>) => {
        dispatch(updateProfile(userData));
        return api('put', '/users/me', userData)
            .then(response => dispatch(updateProfileSuccess(response.data.data, response.data.message)))
            .catch(err => dispatch(updateProfileError(err)))
    }
}

interface UpdateProfileSuccess {
    type: typeof UPDATE_PROFILE_SUCCESS;
    updatedUser: IUser;
    notification: IApiNotification;
}
export function updateProfileSuccess(updatedUser: IUser, message: string): UpdateProfileSuccess {
    return {
        type: UPDATE_PROFILE_SUCCESS,
        updatedUser,
        notification: {
            type: "success",
            message
        },
    }
}

interface UpdateProfileError {
    type: typeof UPDATE_PROFILE_ERROR;
    notification: IApiNotification;
}
export function updateProfileError(error: IApiError<TAuthForm>): UpdateProfileError {
    return {
        type: UPDATE_PROFILE_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

interface ClearAuthError {
    type: typeof CLEAR_AUTH_ERROR;
}
export function clearAuthError(): ClearAuthError {
    return {
        type: CLEAR_AUTH_ERROR,
    }
}

interface ForgotPassword {
    type: typeof FORGOT_PASSWORD;
    email: string;
}
export function forgotPassword(email: string): ForgotPassword {
    return {
        type: FORGOT_PASSWORD,
        email,
    }
}

export function performForgotPassword(email: string) {
    return (dispatch: Dispatch<AuthActions>) => {
        dispatch(forgotPassword(email));
        return api('post', '/password/email', { email })
            .then(response => dispatch(forgotPasswordSuccess(response.data.message)))
            .catch(err => dispatch(forgotPasswordError(err)))
    }
}

interface ForgotPasswordSuccess {
    type: typeof FORGOT_PASSWORD_SUCCESS;
    notification: IApiNotification;
}
export function forgotPasswordSuccess(message: string): ForgotPasswordSuccess {
    return {
        type: FORGOT_PASSWORD_SUCCESS,
        notification: {
            type: "success",
            message
        },
    }
}

interface ForgotPasswordError {
    type: typeof FORGOT_PASSWORD_ERROR;
    notification: IApiNotification;
}
export function forgotPasswordError(error: IApiError<TAuthForm>): ForgotPasswordError {
    return {
        type: FORGOT_PASSWORD_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

interface ResetPassword {
    type: typeof RESET_PASSWORD;
    credentials: Object;
}
export function resetPassword(credentials: Object): ResetPassword {
    return {
        type: RESET_PASSWORD,
        credentials,
    }
}

export function performResetPassword(data: Object) {
    return (dispatch: Dispatch<AuthActions>) => {
        dispatch(resetPassword(data));
        return api('post', '/password/reset', data)
            .then(response => dispatch(resetPasswordSuccess(response.data.message)))
            .catch(err => dispatch(resetPasswordError(err)))
    }
}

interface ResetPasswordSuccess {
    type: typeof RESET_PASSWORD_SUCCESS;
    notification: IApiNotification;
}
export function resetPasswordSuccess(message: string): ResetPasswordSuccess {
    return {
        type: RESET_PASSWORD_SUCCESS,
        notification: {
            type: "success",
            message
        },
    }
}

interface ResetPasswordError {
    type: typeof RESET_PASSWORD_ERROR;
    notification: IApiNotification;
}
export function resetPasswordError(error: IApiError<TAuthForm>): ResetPasswordError {
    return {
        type: RESET_PASSWORD_ERROR,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

export type AuthActions = RequestClientToken
    | RequestClientTokenError
    | RequestClientTokenSuccess
    | Login
    | LoginSuccess
    | LoginError
    | Logout
    | UpdateProfile
    | UpdateProfileSuccess
    | UpdateProfileError
    | ForgotPassword
    | ForgotPasswordSuccess
    | ForgotPasswordError
    | ResetPassword
    | ResetPasswordSuccess
    | ResetPasswordError
    | ClearAuthError