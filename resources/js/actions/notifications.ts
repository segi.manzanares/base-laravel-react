import { IApiNotification } from './../models/apinotification';
import { IApiError } from './../models/apierror';
export const ADD_ERROR_NOTIFICATION = 'ADD_ERROR_NOTIFICATION'

interface AddErrorNotification {
    type: typeof ADD_ERROR_NOTIFICATION;
    notification: IApiNotification;
}
export function addErrorNotification(error: IApiError) {
    return {
        type: ADD_ERROR_NOTIFICATION,
        notification: {
            type: "error",
            message: error.message,
            error
        },
    }
}

export type NotificationsActions = AddErrorNotification