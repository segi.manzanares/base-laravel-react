const Home = () => {
    return (
        <>
            <div className="content-header">
                <div className="container-fluid">
                    Home
                </div>
            </div>
            <section className="content">
                <div className="container-fluid">
                    <h2>Home page</h2>
                </div>
            </section>
        </>
    );
}

export default Home;