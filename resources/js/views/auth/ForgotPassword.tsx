import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { performForgotPassword, requestClientTokenIfNeeded } from '../../actions/auth';
import { RootState } from '../../reducers';
import { TAuthForm } from '../../models/auth';

const ForgotPassword = () => {
    const dispatch = useDispatch()
    const { register, handleSubmit, setError, formState: { errors } } = useForm();
    const [errorMessage, setErrorMessage] = useState<string>(null);
    const authError = useSelector((store: RootState) => {
        return store.auth.error;
    })
    React.useEffect(() => {
        dispatch(requestClientTokenIfNeeded())
    }, []);
    React.useEffect(() => {
        if (authError) {
            setErrorMessage(authError.message)
            if (authError.errors) {
                for (let key in authError.errors) {
                    if (authError.errors.hasOwnProperty(key)) {
                        setError(key, { type: 'validate', message: authError.errors[key][0] })
                    }
                }
            }
        }
    }, [authError])

    const requestPassword = function (data: TAuthForm) {
        setErrorMessage(null)
        dispatch(performForgotPassword(data.email))
    }
    const body = document.getElementsByTagName('body')[0]
    body.classList.remove('hold-transition', 'sidebar-mini', 'layout-fixed')
    body.classList.add('login-page')
    return (
        <div className="login-box">
            <div className="login-logo">
                <img src="/logo192.png" alt="Logo" />
            </div>
            <div className="card">
                <div className="card-body login-card-body">
                    {errorMessage ? <div className="alert alert-danger">
                        <strong>{ errorMessage }</strong>
                    </div>
                    : ''}
                    <p className="login-box-msg">
                        For security reasons, system passwords are encrypted and can not be displayed.
                        <br/><br/>
                        Therefore, if you forgot your password, you must enter your registered email and we will send you a link to reset it.
                    </p>
                    <form onSubmit={handleSubmit(requestPassword)}>
                        <div className="form-group">
                            <input type="email" autoFocus
                                placeholder="E-mail"
                                {...register("email", { required: true })}
                                className={'form-control ' + (errors.email ? 'is-invalid' : '')} />
                            {
                                errors.email && errors.email.type === 'required' &&
                                <span className="invalid-feedback" role="alert"><strong>Por favor completa este campo.</strong></span>
                            }
                            {
                                errors.email && errors.email.type === 'validate' &&
                                <span className="invalid-feedback" role="alert"><strong>{errors.email.message}</strong></span>
                            }
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <button type="submit" className="btn btn-primary btn-block">Solicitar cambio de contraseña</button>
                            </div>
                        </div>
                    </form>
                    <p className="mt-3 mb-1">
                        <Link to="/login">Login</Link>
                    </p>
                    <div className="text-center mt-3">
                        Copyright © Naba Software 2021
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ForgotPassword;