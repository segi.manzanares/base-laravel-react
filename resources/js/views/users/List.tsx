import React, { Fragment, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form';
import { Modal, Button } from 'react-bootstrap';
import { useLocation, useHistory } from 'react-router-dom';
import { loadUsersIfNeeded, fetchUsers, performDeleteUser, performToggleUser, clearUsersError } from '../../actions/users';
import Paginator from '../../components/Paginator';
import UserForm from './Form';
import { showConfirmDialog, handleSort } from '../../utils/helpers'
import api from '../../api'
import { IUser, TUserForm, User } from '../../models/user';
import { ITableOptions } from '../../models/tableoptions';
import { RootState } from '../../reducers';
import { selectUsers, isLoading } from '../../selectors/users';
import { Loader } from '../../components/Loader';
import { IApiError } from '../../models/apierror';
import { addErrorNotification } from '../../actions';

const UsersList = () => {
    const emptyUserData = {
        id: null,
        first_name: null,
        last_name: null,
        email: null,
        password: null,
        role_id: null,
        avatar_path: null
    }
    const [options, setOptions] = useState<ITableOptions>({
        filters: {
            name: null,
            email: null,
        },
        pagination: {
            page: 1,
            take: 20,
        },
        sort: 'name|asc'
    });
    const modalBodyStyle = {
        maxHeight: window.innerHeight - 200
    }
    const { register, handleSubmit } = useForm();
    const [showModal, setShowModal] = useState<boolean>(false);
    const [formMode, setFormMode] = useState<string>("create");
    const [userData, setUserData] = useState<TUserForm>(emptyUserData);
    const users = useSelector(selectUsers)
    const total = useSelector((store: RootState) => {
        return store.users.total;
    })
    const isUsersLoading = useSelector(isLoading)
    const dispatch = useDispatch()
    const location = useLocation()
    const history = useHistory()
    React.useEffect(() => {
        if (location.pathname === "/users") {
            dispatch(loadUsersIfNeeded())
        }
        else if (/\/users\/[0-9]+$/.test(location.pathname)) {
            let userId = location.pathname.replace("/users/", "")
            if (users && users.length > 0) {
                // Buscar record
                let u = users.filter(r => r.id == userId)
                if (u.length > 0) {
                    editUser(u[0])
                }
            }
            else {
                api('get', `/users/${userId}`)
                    .then(r => editUser(r.data))
                    .catch((e: IApiError) => {
                        if (e.status === 404) {
                            dispatch(addErrorNotification(e))
                        }
                    })
            }
        }
        else {
            dispatch(addErrorNotification({ status: 404, message: "Not Found" }))
        }
    }, [])

    const onPageChange = (page: number) => {
        setOptions({
            ...options,
            pagination: { ...options.pagination, page: page }
        });
        dispatch(fetchUsers({ ...options, pagination: { ...options.pagination, page: page } }))
    }

    const handleCloseModal = () => {
        dispatch(clearUsersError());
        setShowModal(false);
        if (formMode === "edit") {
            history.goBack();
        }
    }

    const createUser = () => {
        setFormMode("create")
        setUserData(emptyUserData)
        setShowModal(true)
    }

    const onSubmit = (filters: Object) => {
        const newOptions = {
            ...options,
            filters: { ...filters }
        }
        setOptions(newOptions)
        dispatch(fetchUsers(newOptions))
    }

    const toggleUserStatus = (user: IUser) => {
        dispatch(performToggleUser(user))
    }

    const goEditUser = (user: IUser) => {
        history.push(`/users/${user.id}`)
    }

    const editUser = (user: IUser) => {
        setFormMode("edit")
        setUserData({
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            password: user.password,
            role_id: user.role_id,
            avatar_path: user.avatar?.path
        })
        setShowModal(true)
    }

    const deleteUser = (user: IUser) => {
        showConfirmDialog({
            title: "Eliminar usuario",
            text: "¿Estas seguro de eliminar el usuario seleccionado?",
            icon: 'question',
        }).then(ok => {
            if (ok) {
                dispatch(performDeleteUser(user))
            }
        })
    }

    const onSort = (sortBy: string, direction: string) => {
        const sort = `${sortBy}|${direction}`
        setOptions({ ...options, sort });
        dispatch(fetchUsers({ ...options, sort }))
    }

    return (
        <Fragment>
            <div className="content-header">
                <div className="container-fluid">
                    <h1 className="m-0 text-dark"><strong>Usuarios</strong></h1>
                </div>
            </div>
            <section className="content">
                <div className="card">
                    <div className="card-header">
                        <div className="float-left">
                            <h3 className="card-title">Listado de usuarios</h3>
                        </div>
                        <div className="float-right">
                            <button className="btn btn-primary" onClick={createUser}>
                                <i className="fa fa-plus-square"></i> Crear usuario
                            </button>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    <div className="card-body">
                        <form className="form-inline table-filters" onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <label className="control-label">Nombre</label>
                                <input type="text" name="name" className="form-control" {...register("name")} />
                            </div>
                            <div className="form-group">
                                <label className="control-label">E-mail</label>
                                <input type="text" name="email" className="form-control" {...register("email")} />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-secondary" type="submit">
                                    <i className="fa fa-search"></i> Filtrar
                                </button>
                            </div>
                        </form>
                        <div className="table-responsive">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th sortable="name" onClick={(e) => handleSort(e, onSort)} className="asc">Nombre</th>
                                        <th sortable="email" onClick={(e) => handleSort(e, onSort)}>E-mail</th>
                                        <th width="150" className="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        users.map((user: User, i: number) => {
                                            return <tr key={i}>
                                                <td>{user.fullName}</td>
                                                <td>{user.email}</td>
                                                <td>
                                                    <div className="table-button-container text-center">
                                                        <a href="#" className={'action-link ' + (user.is_active ? 'text-success' : 'text-danger')}
                                                            onClick={(e) => { e.preventDefault(); toggleUserStatus(user) }}>
                                                            <i className="fa fa-check"></i>
                                                        </a>
                                                        <a href="#" className="action-link text-info" onClick={(e) => { e.preventDefault(); goEditUser(user) }}>
                                                            <i className="fa fa-pencil-alt"></i>
                                                        </a>
                                                        <a href="#" className="action-link text-danger" onClick={(e) => { e.preventDefault(); deleteUser(user) }}>
                                                            <i className="fa fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        })
                                    }
                                </tbody>
                            </table>
                            <div>
                                <div className="d-flex justify-content-between p-2">
                                    <Paginator collectionSize={total}
                                        page={options.pagination.page}
                                        pageSize={options.pagination.take}
                                        maxSize={5}
                                        onPageChange={onPageChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>{formMode === 'create' ? "Crear" : "Editar"} usuario</Modal.Title>
                </Modal.Header>
                <Modal.Body style={modalBodyStyle}>
                    <UserForm id="userForm" userData={userData} mode={formMode} />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" form="userForm">
                        Guardar
                    </Button>
                </Modal.Footer>
            </Modal>
            {isUsersLoading ? <Loader type="Puff" color="#00BFFF" /> : ''}
        </Fragment>
    );
}

export default UsersList;