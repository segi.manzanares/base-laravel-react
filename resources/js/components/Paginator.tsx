import { useState, Fragment } from 'react';
import Pagination from 'react-bootstrap/Pagination'
import PropTypes from 'prop-types'

interface IPaginatorProps {
    collectionSize: number,
    pageSize: number,
    page: number,
    maxSize: number,
    onPageChange: (page: number) => void,
}
const Paginator = (props: IPaginatorProps) => {
    const [active, setActive] = useState<number>(props.page)
    const numPages = Math.ceil(props.collectionSize / props.pageSize)
    const maxItems = numPages >= props.maxSize ? props.maxSize : numPages;
    const center = Math.ceil(maxItems / 2);
    let items = [];

    const setPage = (page: number) => {
        setActive(page);
        props.onPageChange(page);
    }
    const goTo = (action: string) => {
        if (action === 'first') {
            setPage(1)
        }
        else if (action === 'last') {
            setPage(numPages)
        }
        else if (action === 'prev') {
            if (active > 1) {
                setPage(active - 1)
            }
        }
        else if (action === 'next') {
            if (active < numPages) {
                setPage(active + 1)
            }
        }
    }
    if (numPages > 1) {
        items.push(<Pagination.First key="first" disabled={active === 1} onClick={() => goTo('first')}/>)
        items.push(<Pagination.Prev key="prev" disabled={active === 1} onClick={() => goTo('prev')}/>)
    }
    const centerPage = Math.ceil(numPages / 2)
    const firstPageItem = (active + center - 1 === numPages - (maxItems - center - 1))
        ? active -1
        : active;
    for (let item = 1; item <= maxItems; item++) {
        let numberPage = item
        if (numPages > maxItems) {
            numberPage = (active <= centerPage) ? firstPageItem + item - 1 : item;
            if (active <= centerPage) {
                if (item === center + 1) {
                    items.push(<Pagination.Ellipsis disabled key="ellipsis" />)
                }
            }
            else if (item === center) {
                items.push(<Pagination.Ellipsis disabled key="ellipsis" />)
            }
            if (active <= centerPage) {
                if (item >= center + 1) {
                    numberPage = numPages - (maxItems - item)
                }
            }
            else if (item >= center) {
                numberPage = active - (maxItems - item)
            }
        }
        items.push(
            <Pagination.Item key={item} active={numberPage === active} onClick={() => setPage(numberPage)}>
                {numberPage}
            </Pagination.Item>,
        );
    }
    if (numPages > 1) {
        items.push(<Pagination.Next key="next" disabled={active === numPages} onClick={() => goTo('next')}/>)
        items.push(<Pagination.Last key="last" disabled={active === numPages} onClick={() => goTo('last')}/>)
    }
    return (
        <Fragment>{ (numPages > 1) ? <Pagination>{items}</Pagination> : '' }</Fragment>
    );
}

Paginator.propTypes = {
    collectionSize: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    maxSize: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired
}

export default Paginator;