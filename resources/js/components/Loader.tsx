import LoaderSpinner from 'react-loader-spinner';

interface ILoaderProps {
    type: "Audio"
        | "BallTriangle"
        | "Bars"
        | "Circles"
        | "Grid"
        | "Hearts"
        | "Oval"
        | "Puff"
        | "Rings"
        | "TailSpin"
        | "ThreeDots"
        | "Watch"
        | "RevolvingDot"
        | "Triangle"
        | "Plane"
        | "MutatingDots"
        | "CradleLoader";
    color: string;
    height?: number;
    width?: number;
}
export const Loader = (props: ILoaderProps) => {
    return (
        <div className="loader">
            <div className="spinner">
                <LoaderSpinner
                    type={props.type}
                    color={props.color}
                    height={props.height ? props.height : 100}
                    width={props.width ? props.width : 100}
                />
            </div>
        </div>
    )
}
