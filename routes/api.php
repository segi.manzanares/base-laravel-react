<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth_client'], 'prefix' => 'v1'], function() {
    // Reestablecer contraseña
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});
Route::group(['middleware' => ['auth:api'], 'prefix' => 'v1', 'namespace' => 'Api'], function() {
    //Logout
    Route::delete('oauth/token', ['uses' => 'AccessTokenController@deleteToken']);
    // Usuarios
    Route::get('users', 'UserController@index')->name('api.v1.users.index');
    Route::post('users', 'UserController@store')->name('api.v1.users.store');
    Route::get('users/me', 'UserController@me')->name('api.v1.users.me');
    Route::put('users/me', 'UserController@updateMe')->name('api.v1.users.updateMe');
    Route::get('users/{id}', 'UserController@show')->name('api.v1.users.show');
    Route::put('users/{id}', 'UserController@update')->name('api.v1.users.update');
    Route::delete('users/{id}', 'UserController@destroy')->name('api.v1.users.destroy');
    Route::put('users/{id}/status', 'UserController@updateStatus')->name('api.v1.users.status');
    // Catálogos
    Route::get('catalogues/{catalogue}', 'CatalogueController@index')->name('api.v1.catalogues');
});
