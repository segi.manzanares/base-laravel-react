<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UnauthorizedHttpException extends HttpException
{
    /**
     * Error code
     * @var string 
     */
    protected $code;

    /**
     * Create a new resource exception instance.
     *
     * @param string                               $message
     * @param \Exception                           $previous
     * @param array                                $headers
     * @param int                                  $code
     *
     * @return void
     */
    public function __construct($message = null, $errorCode = null, Exception $previous = null, $headers = [], $code = 0)
    {
        $this->code = $errorCode;
        parent::__construct(401, $message, $previous, $headers, $code);
    }
}
